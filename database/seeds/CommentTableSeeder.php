<?php

use Illuminate\Database\Seeder;

class CommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        foreach (Ambience\Blog\Post::all() as $post) {
            $comment = new Ambience\Blog\Comment([
                'author' => $faker->name,
                'content' => $faker->realText(100)
            ]);
            $post->comments()->save($comment);
        }
    }
}
