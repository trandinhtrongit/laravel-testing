<?php

use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $faker = Faker\Factory::create();
        
        for($i=1;$i<100;$i++){
            Ambience\Blog\Post::create([
                'title' => $faker->realText(20),
                'content' => $faker->realText()
            ]);
        }
    }
}
