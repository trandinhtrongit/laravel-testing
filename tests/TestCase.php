<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase {

    use \App\Services\ValidatesApi;

    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost:8000';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication() {

        putenv('DB_CONNECTION=sqlite_testing');

        $app = require __DIR__ . '/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    public function setup() {
        parent::setup();
        $this->artisan('migrate:refresh');
        $this->artisan('migrate');
        $this->artisan('db:seed');
    }

    public function tearDown() {
        parent::tearDown();
    }

}
