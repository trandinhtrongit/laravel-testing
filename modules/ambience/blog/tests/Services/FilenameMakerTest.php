<?php

/**
 * Description of FilenameMakerTest
 *
 * @author dinhtrong
 */

use Mockery;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

class FilenameMakerTest extends TestCase {

    public function testFileDoseNotExist() {

        $fileUpload = Mockery::mock(UploadedFile::class);

        $fileUpload->shouldReceive('getClientOriginalName')
                ->once()
                ->andReturn('trong.png');

        $fileUpload
                ->shouldReceive('getClientOriginalExtension')
                ->once()
                ->andReturn('png');

        File::shouldReceive('exists')
                ->once()
                ->andReturn(false);
        
        $fileNameMaker = new \Ambience\Blog\Services\FilenameMaker('path',$fileUpload);
        $name = $fileNameMaker->make();
        $this->assertEquals('trong.png', $name);
    }
    
    public function testFileIsExist1Time() {

        $fileUpload = Mockery::mock(UploadedFile::class);

        $fileUpload->shouldReceive('getClientOriginalName')
                ->once()
                ->andReturn('trong.png');

        $fileUpload
                ->shouldReceive('getClientOriginalExtension')
                ->once()
                ->andReturn('png');

        File::shouldReceive('exists')
                ->andReturn(true,false);
        
        $fileNameMaker = new \Ambience\Blog\Services\FilenameMaker('path',$fileUpload);
        $name = $fileNameMaker->make();
        $this->assertEquals('trong_1.png', $name);
    }
    
    public function testFileIsExist2Times() {

        $fileUpload = Mockery::mock(UploadedFile::class);

        $fileUpload->shouldReceive('getClientOriginalName')
                ->once()
                ->andReturn('trong.png');

        $fileUpload
                ->shouldReceive('getClientOriginalExtension')
                ->once()
                ->andReturn('png');

        File::shouldReceive('exists')
                ->andReturn(true,true,false);
        
        $fileNameMaker = new \Ambience\Blog\Services\FilenameMaker('path',$fileUpload);
        $name = $fileNameMaker->make();
        $this->assertEquals('trong_2.png', $name);
    }

}
