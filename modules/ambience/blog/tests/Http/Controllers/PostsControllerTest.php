<?php

/**
 * Description of PostsControllerTest
 *
 * @author dinhtrong
 */
class PostsControllerTest extends TestCase {

    public function testSureRight() {
        $this->assertTrue(true);
    }

    public function testGetListPostPage1() {
        $this->json('GET', 'api/v1/posts')
                ->seeJson([
                    'total' => 99
                ])
                ->seeValueAtKey('per_page', 20);;
    }
    
    

}
