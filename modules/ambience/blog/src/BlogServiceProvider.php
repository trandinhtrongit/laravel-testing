<?php

namespace Ambience\Blog;

use Illuminate\Support\ServiceProvider;

/**
 * Description of HelloServiceProvider
 *
 * @author dinhtrong
 */
class BlogServiceProvider extends ServiceProvider {

    public function boot() {

        $configPath = __DIR__ . '/config.php';
        $publicPath = __DIR__ . '/../public';

        // Publish config.
        $this->publishes([
            $configPath => config_path('blog.php'),
            $publicPath => public_path('modules/ambience/blog'),
        ]);
    }

    public function register() {
        
        $configPath = __DIR__ . '/config.php';
        $routePath = __DIR__ . '/Http/routes.php';
        $viewPath = __DIR__ . '/../resources/views';

        $this->mergeConfigFrom($configPath, 'blog');

        $this->loadViewsFrom($viewPath, 'blog');

        // Routes
        require $routePath;
    }

}
