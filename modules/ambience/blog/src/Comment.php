<?php

namespace Ambience\Blog;

/**
 * Description of Comment
 *
 * @author dinhtrong
 */
class Comment extends Model {

    protected $fillable = [
        'post_id','author', 'content'
    ];
    
    public function post(){
        return $this->belongsTo(Post::class);
    }

}
