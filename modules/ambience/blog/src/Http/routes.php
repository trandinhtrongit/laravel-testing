<?php

Route::group(['prefix' => 'api/v1'], function() {
    Route::resource('posts',  \Ambience\Blog\Http\Controllers\PostsController::class);
});
