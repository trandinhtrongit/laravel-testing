<?php

namespace Ambience\Blog\Http\Controllers;

use App\Http\Controllers\Controller;
use Ambience\Blog\Post;

/**
 * Description of PostsController
 *
 * @author dinhtrong
 */
class PostsController extends Controller {
    
    protected $post;

    public function __construct(Post $post) {
        $this->post = $post;
    }
    
    public function index(){
        return response()->json($this->post->paginate(20));
    }
    
    public function show($id){
        $post = $this->post->findOrFail($id);
        return response()->json($post);
    }
    
    public function delete($id){
        
    }
    
}
