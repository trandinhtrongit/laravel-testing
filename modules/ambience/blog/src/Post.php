<?php

namespace Ambience\Blog;

/**
 * Description of Post
 *
 * @author dinhtrong
 */
class Post extends Model {
    
    protected $fillable = [
        'title','content'
    ];
    
    public function comments(){
        return $this->hasMany(Comment::class);
    }
}
