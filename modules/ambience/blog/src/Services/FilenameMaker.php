<?php

namespace Ambience\Blog\Services;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

/**
 * Help create clean file name, and auto rename if file exist
 *
 * @author dinhtrong
 */
class FilenameMaker {

    protected $file;
    protected $path;

    public function __construct($path = null, $file = null) {
        if($path){
            $this->path = $path;
        }
        if($file instanceof UploadedFile){
            $this->file = $file;
        }
    }
    
    public function getFile() {
        return $this->file;
    }

    public function getPath() {
        return $this->path;
    }

    public function setFile(UploadedFile $file) {
        $this->file = $file;
    }

    public function setPath($path) {
        $this->path = $path;
    }

    
    public function make() {
        $fileName = self::cleanString($this->file->getClientOriginalName());
        $ext = $this->file->getClientOriginalExtension();
        $onlyName = pathinfo($fileName, PATHINFO_FILENAME);
        return self::getRightFileName($this->path, $fileName, $onlyName, $ext);
    }

    protected static function cleanString($string) {
        $withHyphens = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $noSpecial = preg_replace('/[^A-Za-z0-9\-\._]/', '', $withHyphens); // Removes special chars.
        return preg_replace('/-+/', '-', $noSpecial); // Replaces multiple hyphens with single one.
    }

    protected static function getRightFileName($directory, $fileName, $onlyName, $ext) {

        if (File::exists($directory . '/' . $fileName)) {
            // var_dump("$fileName exist"); for test
            if (preg_match('/_[0-9]+$/', $onlyName, $m)) {
                $next = intval(str_replace("_", "", $m[0])) + 1;
                $ultimateName = preg_replace('/[0-9]+$/', $next, $onlyName);
            } else {
                $ultimateName = $onlyName . '_1';
            }
            $ultimateNameWithExt = $ext ? $ultimateName . "." . $ext : $ultimateName;
            return self::getRightFileName($directory, $ultimateNameWithExt, $ultimateName, $ext);
        } else {
            return $ext ? $onlyName . '.' . $ext : $onlyName;
        }
    }

}
